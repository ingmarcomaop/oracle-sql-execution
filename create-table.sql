CREATE TABLE Customers (
         CustomerId      NUMBER(5) PRIMARY KEY,
         Name            VARCHAR2(15) NOT NULL,
         Location        VARCHAR2(10),
         Email           VARCHAR2(10),
         Address         VARCHAR2(10),
         Age             NUMBER(5)
         )

   STORAGE ( INITIAL 50K);
